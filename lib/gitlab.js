const axios = require('axios');

const BASE_URL = 'https://gitlab.com';

class Api {
  constructor(baseUrl = BASE_URL, headers = {}) {
    if (!baseUrl) throw new Error('Base url is required');
    this.baseUrl = baseUrl;
    this.headers = headers;
  }

  async get(endpoint = '') {
    return (await axios.get(`${this.baseUrl}${endpoint}`, { headers: this.headers })).data;
  }

  async post(endpoint = '', data = {}) {
    return (await axios.post(`${this.baseUrl}${endpoint}`, data, { headers: this.headers })).data;
  }

  async put(endpoint = '', data = {}) {
    return (await axios.put(`${this.baseUrl}${endpoint}`, data, { headers: this.headers })).data;
  }

  async delete(endpoint = '') {
    return (await axios.get(`${this.baseUrl}${endpoint}`, { headers: this.headers })).data;
  }
}

class GitlabApi {
  constructor(accessKey) {
    if (!accessKey) throw new Error('Access key is required');
    this.api = new Api(`${BASE_URL}/api/v4`, { 'PRIVATE-TOKEN': accessKey });
  }

  async init (projectName) {
    if (!projectName) throw new Error('Project name is required');
    try {
      this.user = await this.api.get('/user');
      this.project = await this.api.get(`/projects/${encodeURIComponent(projectName)}`);
      this.variables = await this.api.get(`/projects/${this.project.id}/variables`);
    } catch (error) {
      throw new Error('Invalid project or user identifier, please verify and try again');
    }
  }

  async create (key, value, variableType = 'env_var', environmentScope = '*') {
    try {
      const data = {
        key,
        value,
        protected: false,
        variable_type: variableType,
        masked: false,
        environment_scope: environmentScope,
      };
      return await this.api.post(`/projects/${this.project.id}/variables`, data);
    } catch (error) {
      return await this.update(key, value, variableType, environmentScope);
    }
  }

  async update (key, value, variableType = 'env_var', environmentScope = '*') {
    try {
      const data = {
        key,
        value,
        protected: false,
        variable_type: variableType,
        masked: false,
        environment_scope: environmentScope,
      };
      return await this.api.put(`/projects/${this.project.id}/variables/${key}`, data);
    } catch (error) {
      return null;
    }
  }
  
  async delete (key) {
    try {
      return await this.api.delete(`/projects/${this.project.id}/variables/${key}`);
    } catch (error) {
      return null;
    }
  }

  async recreate (key, value, variableType = 'env_var', environmentScope = '*') {
    try { await this.delete(key); } catch (error) {}
    try {
      return await this.create(key, value, variableType, environmentScope);
    } catch (error) {
      return null;
    }
  }
};

module.exports = GitlabApi;