#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const dotenv = require('dotenv');
const { parsedObj: args } = require('simple-argv-parser');
const GitlabApi = require('../lib/gitlab');

/**
 * Prepare arguments and send them directly to the parser
 * USAGE
 * projectName => Gitlabs project name
 * target => This may be a file, or a token ENVIRONMENT_VARIABLE_NAME
 * (-t | --type) [environment|variable|file]
 * (-v | --value) required if type is [variable|file]
 * (-s | --scope) Optional scope for deployment
 * (-c | --credentials) path to the credentials file | default is ~/.gitlab
 * (-k | --key) access key | this is optional and has priority over the -c option
 * (-o | --option) [recreate|update|create] default is update. 
 *      - recreate => will drop and create
 *      - update => just update (if not found, nothing happens)
 *      - create => will attempt to create
 *      - delete => just for deleting an specified variable
 */
(async function () {
  const argumentKeys = Object.keys(args);
  argumentKeys.forEach(key => { args[key] = args[key] || true });
  const argumentValues = argumentKeys.map(key => args[key]);
  const argumentAll = argumentKeys.concat(argumentValues);

  // Prapare variables
  const projectName = process.argv
    .slice(2)
    .filter(argument => !argumentAll.includes(argument))[0];
  const target = process.argv
    .slice(2)
    .filter(argument => !argumentAll.includes(argument))[1];
  const type = args['-t'] || args['--type'];
  const value = args['-v'] || args['--value'] || null;
  const credentials = args['-c'] || args['--credentials'];
  const key = args['-k'] || args['--key'];
  const option = args['-o'] || args['--option'] || 'update';
  const scope = args['-s'] || args['--scope'] || '*';
  let accessKey = null;
  let targetName = null;

  // Validations
  if (!projectName) {
    console.log('Sorry, yo have to specify a gitlab project name for resolution');
    return;
  }

  if (!target) {
    console.log('No target was specified, please specify one');
    return;
  }

  if (!type) {
    console.log('No type was specified, please specify one');
    return;
  }

  if (!['environment', 'variable', 'file'].includes(type)) {
    console.log('Type can be only [environment|variable|file]');
    return;
  }

  if (['variable', 'file'].includes(type) && !value) {
    console.log('Value must be specified for [variable|file] options');
    return;
  }

  if (!['recreate', 'update', 'create', 'delete'].includes(option)) {
    console.log('Type can be only [recreate|update|create|delete]');
    return;
  }

  if (key) {
    accessKey = key;
  } else if (credentials) {
    const filePath = credentials[0] === '/' ? credentials : path.join(__dirname, credentials);
    try {
      accessKey = fs.readFileSync(filePath, 'utf8');
    } catch (error) {
      accessKey = null;
    }
  } else {
    try {
      const filePath = path.join(process.env.HOME, '.gitlab');
      accessKey = fs.readFileSync(filePath, 'utf8');
    } catch (error) {
      console.log(error);
      accessKey = null;
    }
  }

  if (!accessKey || typeof accessKey !== 'string' || accessKey.trim().length === 0) {
    console.log('No gilab credentials were found');
    return;
  } else {
    // Clean file from strange characters
    accessKey = accessKey.replace(/(\n|\s)/g, '');
  }

  if (type !== 'variable') {
    targetName = target[0] === '/' ? target : path.join(process.env.PWD, target);
  } else {
    targetName = target;
  }

  if (!targetName || typeof targetName !== 'string' || targetName.trim().length === 0) {
    console.log('Target name could not be found');
    return;
  }

  await runClient(projectName, targetName, value, type, scope, option, accessKey);
  console.log(`Variables ${option}d successfully`);
})();


async function runClient (projectName, targetName, value, type, scope = '*', option, accessKey) {
  try {
    const client = new GitlabApi(accessKey);
    await client.init(projectName);

    // Prepare variables
    const variables = [];
    if (type === 'environment') {
      const env = dotenv.config({ path: targetName });
      if (env.error) {
        console.log('There is an error on your environment file, please review it');
        return;
      }
      Object.keys(env.parsed)
        .map(key => ({
          key,
          value: env.parsed[key],
          variableType: 'env_var',
        }))
        .forEach(variable => variables.push(variable));
    } else if (type === 'file') {
      const file = fs.readFileSync(targetName, 'utf8');
      variables.push({
        key: value,
        value: file,
        variableType: 'file',
      });
    } else if (type === 'variable') {
      variables.push({
        key: targetName,
        value: value,
        variableType: 'env_var',
      });
    }

    // Process each variable
    for (const variable of variables) {
      console.log(`Processing variable [${variable.key}]`);
      await client[option](variable.key, variable.value, variable.variableType, scope);
    }
  } catch (error) {
    console.log(error.message);
    return;
  }
}